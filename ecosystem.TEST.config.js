//https://pm2.keymetrics.io/docs/usage/expose/

module.exports = {
    name: "TEST - Start2Impact - StaticWebPage",
    script: "serve",
    env: {
        PM2_SERVE_PATH: '.',
        PM2_SERVE_PORT: 5000
    }
  }